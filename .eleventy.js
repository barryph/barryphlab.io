const inputDir = 'src/site';
const componentsDir = `${inputDir}/_includes/components`;

const syntaxHighlight = require('@11ty/eleventy-plugin-syntaxhighlight');
// Components
const Wrapper = require(`./${componentsDir}/Wrapper.js`);
const Image = require(`./${componentsDir}/Image.js`);
const Button = require(`./${componentsDir}/Button.js`);
const Card = require(`./${componentsDir}/Card.js`);
const Project = require(`./${componentsDir}/Project.js`);

module.exports = function (config) {
	// Code syntax highlighting (prism)
	config.addPlugin(syntaxHighlight);

	// Passthroughs
	config.addPassthroughCopy('src/assets');

	// Filters
	config.addFilter('json', (object) => JSON.stringify(object, null, 2));
	config.addFilter('year', (date) => new Date(date).getFullYear());
	config.addFilter('articleDate', (date) => {
		const dtf = new Intl.DateTimeFormat('en', { year: 'numeric', month: 'long', day: 'numeric' });
		const [{ value: month },, { value: day },, { value: year }] = dtf.formatToParts(new Date(date));
		return `${day} ${month} ${year}`;
	});
	config.addFilter('limit', (arr, limit) => arr.slice(0, limit));
	config.addFilter('category', (posts, category) => posts.filter(post => {
		if (!category) return posts;
		return post.data.categories.includes(category);
	}));

	// Projects
	config.addCollection('publishedProjects', (collection) => {
		return collection.getFilteredByTag('project')
			.filter((project) => !project.data.unpublished);
	});

	config.addCollection('featuredProjects', (collection) => {
		return collection.getFilteredByTag('project')
			.filter((project) => !project.data.unpublished)
			.filter((project) => project.data.featured);
	});

	// Collections
	config.addCollection('categories', (collection) => {
		const categoriesSet = collection.getFilteredByTag('post')
			.reduce((categories, post) => {
				if (post.data.categories) {
					post.data.categories.forEach(category => categories.add(category));
				}
				return categories;
			}, new Set());

		return Array.from(categoriesSet);
	});

  // Paired shortcodes
  config.addPairedShortcode('Wrapper', Wrapper);

  // Shortcodes
  config.addShortcode('Image', Image);
  config.addShortcode('Button', Button);
  config.addShortcode('Card', Card);
  config.addShortcode('Project', Project);

  return {
    dir: {
      input: inputDir,
      output: 'dist',
			includes: '_includes',
			data: '_data',
    },
		passthroughFileCopy: true,
  };
};
