---
tags: project
title: Rachio Portal Dashboard
description: Tracking distrubution and usage of rachio smart sprinklers. Collected analytics are used to demonstrate the programs success to investors.
featured: true
categories:
  - development
  - design
tools:
  - Node.js
  - Passport.js
  - React
  - Redux
responsibility: I was solely responsible for the development and maintenance along with shared responsibility for design.
date: 2019-01-01
---

Easily managable for internal team members to manage, release and alter programs.
Authentication
Clear feedback for municipalities to see the success of programs they're funding.
Easily searchable for their support team to diagnose issues. Accurate record of submissions.
