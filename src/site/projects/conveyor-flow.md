---
tags: project
title: Conveyor Flow
description: Automates calculating the dimensions of conveyor pulley systems. Generates DXF drawings and the bill of materials. Developed in Dash with Flask for authentication.
order: 10
featured: true
categories:
  - development
  - design
tools:
  - Python
  - Dash
  - Flask
responsibility: Added a secure authentication guard around the apps dashboard, restricting access to registered users. Revised the apps design for a professional appearance.
date: 2020-01-01
---
