---
tags: project
title: Bear Ltd
description: Online beauty & skin care store. Built with Nuxt.js and shopifys storefront API to achieve a fully custom front-end.
link: https://www.bearltd.com/
categories:
  - development
tools:
  - Vue
  - Nuxt
  - Shopify
  - Dato
  - Tailwind.css
responsibility: Connecting the front-end to shopify, managing interactions between the app and shopifys storefront API to allow the static website to operate as an online store. I also integrated shopifys accounts system allowing users to register and securely maintain their personal customer details.
date: 2020-01-04
---
