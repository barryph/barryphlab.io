---
tags: project
title: Kalaurie
description: Online womenswear fashion store. Built with Nuxt.js and shopifys storefront API to achieve a fully custom front-end.
link: https://www.kalaurie.com.au/
featured: true
order: 5
categories:
  - development
tools:
  - Vue
  - Nuxt
  - Shopify
  - Dato
  - Tailwind.css
responsibility: Connecting the front-end to shopify, managing interactions between the app and shopifys storefront API to allow the static website to operate as an online store.
date: 2020-01-06
---
