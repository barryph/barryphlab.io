---
tags: project
title: Marquee
description: Offical website of the cross platform marquee note application. Includes branding and project hosting. Markdown notes, kept simple.
link: https://heuristic-shirley-12b2b2.netlify.com/
featured: true
categories:
  - development
  - design
tools:
  - Hugo
  - HTML
  - CSS
  - Electron
responsibility: Full design and development
date: 2018-01-01
---
