---
tags: project
title: Network Architectural
description: Brochure website for Network Architectural to show off their products and past projects.
link: https://www.networkarchitectural.com.au/
categories:
  - development
tools:
  - Vue
  - Wordpress
  - Gridsome
  - Tailwind.css
responsibility: Build a static website based on the provided designs. Pull all content from the Wordpress headless API.
date: 2019-01-01
---
Network Architectural wanted a fast and easily manageable website to demonstrate their capabilities to potiental clients.

Wordpress was their choosen CMS as it is for many. Wordpress powers an impressive portion of the internet but it's downfall is often in performance. We needed to lower, or better yet remove, the wait time for wordpress. Our solution was a SSG called gridsome. Using gridsome we generate a static version of the website while still pulling all content from Wordpress, via it's headless API. Thus removing waiting for Wordpress from the equation and leaving the website lightning fast. 
