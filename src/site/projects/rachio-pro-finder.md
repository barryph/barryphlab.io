---
tags: project
title: Rachio Pro Finder
description: Connects Rachio smart sprinkler owners in need of service with Rachio professionals in their area, eliminating nearly all related support tickets.
featured: true
categories:
  - development
tools:
  - React
  - Redux
  - Mapbox
responsibility: The goal was to decrease the quantity of support tickets submitted. I took on full development and built a searchable map with mapbox.
date: 2019-01-01
---
The goal was to decrease the number of tickets submitted to the rachio support team. The Rachio Pro Finder provided a straight forward process for connecting smark sprinkler owners with Certified Rachio Pros, searchable by location and service. 
From the interactive map owners select their local rachio pro along with the type of service requried to request a quote.

With release of the Rachio Pro Finder we provided 72% search coverage of Pros and the number of support tickets from owners in search of a Pro decrease to **almost zero**.
