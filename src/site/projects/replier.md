---
tags: project
title: Replier
description: Replier helps you reply to friends, clients, colleagues and collaborators by choosing only what’s important in the conversation.
link: https://www.replier.app
featured: true
order: 1
categories:
  - development
tools:
  - Vue
  - Prosemirror
  - Tailwind.css
  - PostgreSQL
  - Node.js
responsibility: Full development of the app outside of styling. This included extending a custom text editor, formatting exports for other programs such as Gmail, Slack & Basecamp and an account system including subscription payments (replier has since been released publicly and is free to use!)
date: 2020-01-01
---
Replier helps you reply to friends, clients, colleagues and collaborators by choosing only what’s important in the conversation.

Replier creates focus by removing the superfluous. It helps you avoid losing important info to the vortex of chat messages and email chains. And makes it easy for the reader.

We also give you custom formatted responses to paste into your favourite project management apps. So it’s clear who said what.

Replier works in four basic steps:

1. Click on the line you wish to reply to.
1. Join sentences by holding “command” while you click, anywhere across the doc.
1. Hit the reply button and write your replies.
1. Choose to export to Gmail, Slack or Basecamp.
