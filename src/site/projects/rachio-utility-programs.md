---
tags: project
title: Rachio Municipality Programs
description: Facilitates the claiming of rachio smart sprinkler controllers, distributed as part of special offers by municipality partners. Included a custom dashboard to demonstrate growth and success rates to investors.
link: https://go.rachio.com/lvmwd/
categories:
  - development
tools:
  - React
  - Gatsby
date: 2019-01-01
---
