module.exports = project => (`
	<div class="project">
		<div class="project__image">
			<div class="project__image_filter"></div>
			<img src="${project.image}" />
			<a class="aspect-ratio-box" href="${project.url}">
				<div class="aspect-ratio-box__inner project__img" style="background-image: url('/src/assets/img/responsive/${project.title.toLowerCase()}_thumbnail.webp')">
				</div>
			</a>
		</div>
		<div class="project__body">
			<div class="project__headline">project.title</div>
			<div class="project__bar">
				<div class="project__tags">
				${!project.categories ? '' : project.categories.split(' ').map(category => `
					<span>${category}</span>
				`).join(' ')}
				</div>
				<div class="project__date">
					${new Date(project.date).getFullYear()}
				</div>
			</div>
		</div>
	</div>
`);
