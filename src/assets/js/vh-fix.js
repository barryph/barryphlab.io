/*
 * VH Fix
 *
 * Mobile browsers hide the address bar (or other UI elements) on scoll.
 * The change in size of the browser UI changes the value of vh and causes the page to jolt.
 * This script sets the css variable --hr to a static value calculated as 1% of the window height at run time.
 */
const vh = window.innerHeight * 0.01;
document.documentElement.style.setProperty('--vh', `${vh}px`);
