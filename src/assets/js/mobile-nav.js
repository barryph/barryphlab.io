function getAnimationDuration(elem) {
	var rawAnimationDuration = getComputedStyle(elem).getPropertyValue('--animation-duration');
	return rawAnimationDuration.substring(0, rawAnimationDuration.length - 2);
}

var mobileNavButtons = document.getElementsByClassName("js--mobile-nav-button");
var mobileNav = document.getElementsByClassName("js--mobile-nav")[0];
var animationDurationInMS = getAnimationDuration(mobileNav);

function openNav(mobileNavButton) {
	// Lock scroll
	document.body.style.overflow = 'hidden';

	mobileNav.classList.remove("hidden");

	// Short timeout for .hidden styles have been removed
	// Otherwise the browser does not animate to the .open styles
	setTimeout(() => {
		mobileNav.classList.add("open");
		mobileNavButton.classList.add("open");
	}, 10);
}

function closeNav(mobileNavButton) {
	// Unlock scroll
	document.body.style.overflow = 'initial';

	mobileNav.classList.remove("open");
	mobileNavButton.classList.remove("open");

	// Wait for out animation to finish before hiding
	setTimeout(() => {
		mobileNav.classList.add("hidden");
	}, animationDurationInMS);
}

for (mobileNavButton of mobileNavButtons) {
	((mobileNavButton) => {
		mobileNavButton.addEventListener("click", (event) => {
			const isNavOpen = mobileNav.classList.contains("open");

			if (isNavOpen) {
				closeNav(mobileNavButton);
			} else {
				openNav(mobileNavButton);
			}
		});
	})(mobileNavButton);
}
