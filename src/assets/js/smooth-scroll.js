const smoothLinks = document.querySelectorAll('[data-smooth-scroll]');

for (link of smoothLinks) {
	const id = link.getAttribute('data-smooth-scroll');
	const target = document.getElementById(id);

	link.addEventListener('click', (event) => {
		event.preventDefault();

		window.scrollTo({
			top: target.offsetTop - 50,
			behavior: 'smooth',
		});
	});
}
