function getAnimationDuration(elem) {
	var rawAnimationDuration = getComputedStyle(elem).getPropertyValue('--animation-duration');
	return rawAnimationDuration.substring(0, rawAnimationDuration.length - 2);
}

var siteHeader = document.getElementsByClassName("site-header")[0];
var animationDurationInMS = getAnimationDuration(siteHeader);

function showNav() {
	siteHeader.classList.remove("hidden");

	// Short timeout for .hidden styles have been removed
	// Otherwise the browser does not animate to the .open styles
	setTimeout(() => {
		siteHeader.classList.add("open");
	}, 10);
}

function hideNav() {
	siteHeader.classList.remove("open");

	// Wait for out animation to finish before hiding
	setTimeout(() => {
		siteHeader.classList.add("hidden");
	}, animationDurationInMS);
}

var lastScrollPosition = window.scrollY;
var visibilityHeight = 500;

window.addEventListener("scroll", () => {
	const isNavShowing = !siteHeader.classList.contains("hidden");
	// Scroll down past threshold height
	if (window.scrollY >= visibilityHeight &&
	  !isNavShowing) {
		showNav();
	}

	// Scroll up past threshold height
	if (window.scrollY < visibilityHeight &&
		isNavShowing) {
		hideNav();
	}

	lastScrollPosition = window.scrollY;
});
