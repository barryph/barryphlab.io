module.exports = {
	root: true,
	env: {
		browser: true,
	},
	extends: [
		'airbnb-base',
	],
	rules: {
		'quotes': ['error', 'single'],
		'indent': ['error', 'tab'],
		'linebreak-style': ['error', 'unix'],
		'no-tabs': 'off',
		'no-underscore-dangle': 'off',
		'no-plusplus': 'off',
	},
};
