/*
* Not a part of build process so it can be run locally
* then images get committed to the repo. This way deployments
* don't include slow image building.
*
* @module images
*/

/* eslint-disable import/no-extraneous-dependencies */
const fs = require('fs').promises;
const path = require('path');
const sharp = require('sharp');
const imagemin = require('imagemin');
const imageminMozjpeg = require('imagemin-mozjpeg');
/* eslint-enable import/no-extraneous-dependencies */

async function generateImages(
	originDirectory,
	destDirectory,
	widths,
	thumbnail = false,
	thumbnailWidth = 640,
	// thumbnailAspect = 9 / 16,
) {
	// eslint-disable-next-line no-console
	console.log(`Generating images: ${originDirectory}`);
	const quality = 85;
	const files = await fs.readdir(originDirectory);

	await fs.mkdir(destDirectory, { recursive: true });

	const resizePromises = files.map(async (file) => {
		const filePath = originDirectory + file;
		const ext = path.extname(filePath);
		const basename = path.basename(filePath, ext);

		const imagePromises = [];

		if (Array.isArray(widths)) {
			// Multiple widths specified
			const widthsPromises = widths.map(width => sharp(filePath)
				.resize({ width })
				.webp({ quality: width > 1000 ? 90 : quality })
				.toFile(`${destDirectory}${basename}_${width}.webp`));

			imagePromises.concat(widthsPromises);
		} else {
			// Single widths specified
			const imagePromise = sharp(filePath)
				.resize({ width: widths })
				.webp({ quality })
				.toFile(`${destDirectory}${basename}.webp`);
			imagePromises.push(imagePromise);
		}


		if (thumbnail) {
			// const thumbnailHeight = Math.round(thumbnailWidth * thumbnailAspect);
			const thumbnailPromise = sharp(filePath)
				.resize({
					width: thumbnailWidth,
					// height: thumbnailHeight,
					position: 'top',
				})
				.webp({ quality })
				.toFile(`${destDirectory}${basename}_thumbnail.webp`);

			imagePromises.push(thumbnailPromise);
		}

		return Promise.all(imagePromises);
	});

	await Promise.all(resizePromises);
	/*
	 * Compressing jpg isn't worth the CPU cycles, jpgs are already compressed
	await imagemin([`${destDirectory}*.jpg`], destDirectory, {
		plugins: [imageminMozjpeg({
			quality: 90,
		})],
	});
	*/
}
async function main() {
	// Image directories
	const imgDirectory = 'src/assets/img/';
	const originDirectory = `${imgDirectory}original/`;
	const destDirectory = `${imgDirectory}responsive/`;
	// Image sizes
	const widths = [1400, 960, 480];

	await generateImages(`${originDirectory}projects/`, `${destDirectory}projects/`, widths, true);
	await generateImages(`${originDirectory}blog/`, `${destDirectory}blog/`, 1400, true);
	await generateImages(`${originDirectory}misc/`, `${destDirectory}misc/`, 1400);
}

main();
